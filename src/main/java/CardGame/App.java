package CardGame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The app class extends Application Loads and starts the the program
 */
public class App extends Application {

    /**
     * A method to start the program
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(loadFXML("CardGameMainPage"));
        stage.setScene(scene);
        stage.setTitle("CardGame");
        stage.show();
    }


    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }


    /**
     * Method used for loading fxml file
     * @param fxml the name of the fxml file
     * @return
     * @throws IOException
     */
    private static Parent loadFXML(String fxml) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/JavaFX/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

}