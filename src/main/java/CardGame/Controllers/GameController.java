package CardGame.Controllers;

import CardGame.DeckOfCards;
import CardGame.Hand;
import CardGame.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import java.io.FileNotFoundException;

/**
 * Controller for the FXML file. Has dealHand button and CheckHand button. along with some of the fuctions which are used in those actions.
 */
public class GameController {


    @FXML Text sumOfFacesText;
    @FXML Text flushText;
    @FXML Text cardsOfHeartsText;
    @FXML Text queenOfSpadesText;
    @FXML HBox CardBox;
    Hand yourHand;
    DeckOfCards myDeck;

    /**
     * Deal Hand of cads to the player
     * @throws FileNotFoundException
     */
    public void dealHandToPlayer() throws FileNotFoundException {
        myDeck = new DeckOfCards();
        this.yourHand = new Hand(myDeck.dealHand(8));

    }

    /**
     * Checksthe hand. it makes the cards show up in the Hbox, and also analyzses the cards for flushes, queenOfSpades, and hearth cards.
     * also sets the text bars in the FXML file
     * @throws FileNotFoundException
     */
    public void CheckHandButton() throws FileNotFoundException {
        CardBox.getChildren().clear();
        setSumOfFacesText();
        setFlushText();
        setQueenOfSpadesText();
        setCardsOfHeartsText();
        DisplayCards();
    }

    /**
     * Displays the cards in the Hbox
     * @throws FileNotFoundException
     */
    public void DisplayCards() throws FileNotFoundException {
        Image wholeimage = new Image("./DeckOfCards.jpg");

        for(int s =0;s<yourHand.checkHand().size();s++){
            yourHand.checkHand().get(s).setRightImage();
            ImageView imageViewer = new ImageView();
            imageViewer.setImage(wholeimage);
            imageViewer.setViewport(yourHand.checkHand().get(s).CardPicture);
            CardBox.getChildren().add(imageViewer);
        }

    }

    /**
     * gives total sum of all the faces
     */
    public void setSumOfFacesText(){
        sumOfFacesText.setText(String.valueOf(yourHand.getSumOfFaces()));
    }

    /**
     * gives output regarding if the ArrayOfCards has a flush or not.
     */
    public void setFlushText(){
        if(yourHand.flush()){
            flushText.setText("Yes");
        }
        else{
            flushText.setText("No, git good");
        }
    }

    /**
     * gives output regarding if the ArrayOfCards has a queen of spades, or not.
     */
    public void setQueenOfSpadesText(){
        if(yourHand.queenOfSpades()){
            queenOfSpadesText.setText("Yes");
        }
        else{
            queenOfSpadesText.setText("No, git good");
        }
    }

    /**
     * gives output regarding the cards that are of the suit heart in your hand.
     */
    public void setCardsOfHeartsText(){
        if(yourHand.onlyHearts().size()==0){
            cardsOfHeartsText.setText("No hearts this round");
        }
        else{
            StringBuilder textWithHearts = new StringBuilder();
            for(PlayingCard c: yourHand.onlyHearts()){
                textWithHearts.append(c.getAsString()).append("  ");
            }
            cardsOfHeartsText.setText(textWithHearts.toString());
        }
    }

}
