package CardGame;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Class that provies all the cards noramlly found in a deck of cards.
 * also can deal a hand, by giving out a array of random cards.
 */
public class DeckOfCards {

    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private final int[] face = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    public ArrayList<PlayingCard> deck = new ArrayList();

    /**
     * constructor
     * @throws FileNotFoundException
     */
    public DeckOfCards() throws FileNotFoundException {

        for (char s : suit) {
            for (int i : face) {
                deck.add(new PlayingCard(s,i));
            }
        }
    }

    /**
     * Gives out random cards from the deck. it gives out as many cards as u enter in.
     * @param i
     * @return
     */
    public ArrayList<PlayingCard> dealHand(int i){//i is a number between 0 and 52
        if(i>52||i<0){
            return null;
        }
        ArrayList<PlayingCard> selectedCards = new ArrayList();
        Random rng = new Random();
        for(int j=0;j<i;j++){
            int random = rng.nextInt(deck.size());
            selectedCards.add(deck.get(random));
            deck.remove(random);
        }

        return selectedCards;
    }

}
