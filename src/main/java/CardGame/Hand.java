package CardGame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hand {
    ArrayList<PlayingCard> cardsOnHand;

    public Hand (ArrayList<PlayingCard> selectedcards){
        cardsOnHand = new ArrayList<>();
        cardsOnHand = selectedcards;
    }

    public int getSumOfFaces(){
       return cardsOnHand.stream().mapToInt(PlayingCard::getFace).sum();

    }

    public List<PlayingCard> onlyHearts(){
        return cardsOnHand.stream().filter(PlayingCard::isHearts).collect(Collectors.toList());
    }

    public ArrayList<PlayingCard> checkHand(){
        return cardsOnHand;
    }

    public boolean queenOfSpades(){
        return cardsOnHand.stream().anyMatch(PlayingCard::isQueenOfSpades);
    }
    public boolean flush(){
        if(cardsOnHand.stream().filter(PlayingCard::isHearts).count() >=5){
            return true;
        } else if(cardsOnHand.stream().filter(PlayingCard::isDimonds).count() >=5){
            return true;
        } else if(cardsOnHand.stream().filter(PlayingCard::isSpades).count() >=5){
            return true;
        }else return cardsOnHand.stream().filter(PlayingCard::isClover).count() >= 5;
    }
}
