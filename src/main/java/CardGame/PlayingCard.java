package CardGame;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Represents a playing card. A playing card has a number (face) between
 * 1 and 13, where 1 is called an Ace, 11 = Knight, 12 = Queen and 13 = King.
 * The card can also be one of 4 suits: Spade, Heart, Diamonds and Clubs.
 *
 * @author ntnu
 * @version 2020-01-10
 */
public class PlayingCard {

    private final char suit; // 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private final int face; // a number between 1 and 13
    public javafx.geometry.Rectangle2D CardPicture;
    /**
     * Creates an instance of a PlayingCard with a given suit and face.
     *
     * @param suit The suit of the card, as a single character. 'S' for Spades,
     *             'H' for Heart, 'D' for Diamonds and 'C' for clubs
     * @param face The face value of the card, an integer between 1 and 13
     */
    public PlayingCard(char suit, int face) {
        this.suit = suit;
        this.face = face;

    }

    public void setRightImage() {
        Image wholeimage = new Image("./DeckOfCards.jpg");
        ImageView imageViewer = new ImageView();
        imageViewer.setImage(wholeimage);
        javafx.geometry.Rectangle2D imagePart = new javafx.geometry.Rectangle2D((wholeimage.getWidth()/13)*(face-1),(wholeimage.getHeight()/4)*(getSuitAsInt()-1),(wholeimage.getWidth()/13),(wholeimage.getHeight()/4));
        imageViewer.setViewport(imagePart);
        this.CardPicture = imagePart;
        }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        return String.format("%s%s", suit, face);
    }

    /**
     * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for clubs
     *
     * @return the suit of the card
     */
    public char getSuit() {
        return suit;
    }

    public int getSuitAsInt(){
        switch (getSuit())
        {
            case 'S':
                return 1;
            case 'H':
                return 2;
            case 'D':
                return 3;
            default:
                return 4;

        }
    }
    /**
     * Returns the face of the card (value between 1 and 13).
     *
     * @return the face of the card
     */
    public int getFace() {
        return face;
    }


    /**
     * checks if the card is a heart
     * @return
     */
    public boolean isHearts(){
        return suit == 'H';
    }
    /**
     * checks if the card is a clover
     * @return
     */
    public boolean isClover(){
        return suit == 'C';
    }
    /**
     * checks if the card is a spades
     * @return
     */
    public boolean isSpades(){
        return suit == 'S';
    }
    /**
     * checks if the card is a dimond
     * @return
     */
    public boolean isDimonds(){
        return suit == 'D';
    }
    /**
     * checks if the card is a queen of spades
     * @return
     */
    public boolean isQueenOfSpades(){
        return suit == 'S' && (12 == face);
    }
}