package CardGame;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 */
class DeckOfCardsTest {

    @Test
    void dealHand() throws FileNotFoundException {
        DeckOfCards myDeck1 = new DeckOfCards();
        assertNull(myDeck1.dealHand(10523));

        Hand MyHand = new Hand(myDeck1.dealHand(2));
        assertEquals(MyHand.checkHand().size(), 2);

        assertNull(myDeck1.dealHand(-2));
    }
}