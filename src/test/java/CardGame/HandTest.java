package CardGame;

import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {




    HandTest() {
    }

    @Test
    void getSumOfFaces() throws FileNotFoundException {
        DeckOfCards myDeck1 = new DeckOfCards();
        Hand myhand = new Hand(myDeck1.dealHand(0));
        assertEquals(myhand.getSumOfFaces(), 0);

        DeckOfCards myDeck2 = new DeckOfCards();
        Hand myhandOverload = new Hand(myDeck2.dealHand(52));
        assertEquals(myhandOverload.getSumOfFaces(), 364);
    }


    @Test
    void onlyHearts() throws FileNotFoundException {
        ArrayList hearthsOnly= new ArrayList();
        for(int i= 0; i<13; i++){
           PlayingCard Temp = new PlayingCard('H',i+1);
           hearthsOnly.add(Temp);
        }
        Hand myhand = new Hand(hearthsOnly);
        assertEquals(myhand.onlyHearts(), hearthsOnly);

        ArrayList<PlayingCard> empty = new ArrayList();
        Hand emptyhand = new Hand(empty);
        assertEquals(empty, emptyhand.onlyHearts());



    }

    @Test
    void checkHand() throws FileNotFoundException {
        ArrayList checkhandTestarray = new ArrayList();
        PlayingCard temp = new PlayingCard('H',3);
        checkhandTestarray.add(0,temp);
        Hand myhand = new Hand(checkhandTestarray);
        assertTrue(myhand.checkHand().equals(checkhandTestarray));
    }

    @Test
    void queenOfSpades() throws FileNotFoundException {
        ArrayList<PlayingCard> queenOfSpadesArray = new ArrayList();
        PlayingCard queenOfSpades = new PlayingCard('S',12);
        queenOfSpadesArray.add(0,queenOfSpades);
        Hand myhand = new Hand(queenOfSpadesArray);
        assertTrue(myhand.queenOfSpades()==true);

        ArrayList<PlayingCard> notqueenOfSpadesArray = new ArrayList();
        PlayingCard notqueenOfSpades = new PlayingCard('D',3);
        queenOfSpadesArray.add(0,notqueenOfSpades);
        Hand myhand2 = new Hand(notqueenOfSpadesArray);
        assertTrue(!myhand2.queenOfSpades());
    }

    @Test
    void flush() throws FileNotFoundException {
        ArrayList<PlayingCard> flush = new ArrayList();
        for(int i = 0; i<5;i++){
            PlayingCard temp = new PlayingCard('S',i);
            flush.add(temp);
        }
        Hand myhand = new Hand(flush);
        assertTrue(myhand.flush()==true);

        ArrayList<PlayingCard> notFlush = new ArrayList();
        for(int j = 0; j<2; j++){
            PlayingCard temp2 = new PlayingCard('S',j);
            notFlush.add(temp2);
        }
        Hand myhand2 = new Hand(notFlush);
        assertTrue(myhand2.flush()==false);

    }
}